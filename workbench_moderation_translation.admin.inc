<?php

/**
 * Menu callback; displays the WME_translation_publish module settings page.
 *
 * @see system_settings_form()
 */
function workbench_entity_translation_admin_settings_form($form, &$form_state) {
  
  $form = array();

  $form['wmet_publish'] = array(
      '#type' => 'checkbox',
      '#title' => t('Merge entity translation publish with workbench moderation'),
      '#description' => 'If you want to publish entity translation with workbench published state then enable this option.',
      '#default_value' => variable_get('wmet_publish', ''),
  );
  
  // Make a system setting form and return.
  return system_settings_form($form);
}
