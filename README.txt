
WME moderation translation module
-----------------

WME stands for Workbench moderation entity translation status.

This module will help to publish the entity revision according to the moderation revision state.

Installation
------------

First, go to  Home » Administration » modules

Enable Workbench translation publish module.

Second go to Home » Administration » configuration Workbench » Moderation Entity Translation Publish

Enable "Merge entity translation publish with workbench moderation" and save the configuration.

Installation done.

Information
-----------

This will automatically make the entity revision to be published based on the moderation status.

Earlier entity translation module makes the entity translation revision to be published irrespective of the moderation status i.e. draft, review
or published.

Therefore, this module will helps us to make the entity translation revision to be published only if moderation status is set as published.